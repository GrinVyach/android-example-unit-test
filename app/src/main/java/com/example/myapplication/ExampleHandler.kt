package com.example.myapplication

class ExampleHandler {
    fun someMethod(someData: List<Int>): Int {
        var sum: Int = 0
        someData.forEach {
            sum += it
        }

        return sum
    }
}