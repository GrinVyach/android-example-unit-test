package com.example.myapplication

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest2 {
    @Test
    fun right_list_sum_correct() {
        assertEquals(ExampleHandler().someMethod(arrayListOf(1, 2)), 3)
    }
    @Test
    fun empty_list_sum_correct() {
        assertEquals(ExampleHandler().someMethod(arrayListOf()), 0)
    }
    @Test
    fun right_list_sum_correct_first() {
        assertEquals(ExampleHandler().someMethod(arrayListOf(1, 2, 3, 4)), 10)
    }
    @Test
    fun right_list_sum_correct_second() {
        assertEquals(ExampleHandler().someMethod(arrayListOf(2 * 2, 2, 3, 4)), 13)
    }
    @Test
    fun right_list_sum_correct_third() {
        assertEquals(ExampleHandler().someMethod(arrayListOf(3, 4)), 7)
    }
}